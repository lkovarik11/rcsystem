<?php
/**
 * Created by PhpStorm.
 * User: lukaskovarik
 * Date: 16.04.18
 * Time: 18:31
 */

namespace App\Repository;

use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class RoleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Role::class);
    }

    public function getRoles () : array {
        return $this->findAll();
    }

    /**
     * Return roel by ID
     * @param string $id Role id
     * @return null|Role
     */
    public function getRole(int $id) : ?Role
    {
        return $this->findOneBy(['id' => $id]);
    }
}