<?php
/**
 * Created by PhpStorm.
 * User: lukaskovarik
 * Date: 16.04.18
 * Time: 18:31
 */

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUsers () : array {
        return $this->findAll();
    }

    /**
     * Vrátí článek z databáze podle jeho URL.
     * @param string $id User id
     * @return null|User první článek, který odpovídá URL nebo null při neúspěchu
     */
    public function getUser(int $id) : ?User
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * Uloží článek do systému. Pokud není nastaveno ID, vloží nový, jinak provede editaci.
     * @param User $user článek
     */
    public function saveUser(User $user)
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush($user);
    }

    /**
     * Odstraní článek.
     * @param int $id URL článku
     */
    public function removeUser(int $id)
    {
        if (($user = $this->getUser($id))) {
            $this->getEntityManager()->remove($user);
            $this->getEntityManager()->flush();
        }
    }
}