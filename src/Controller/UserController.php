<?php
/**
 * Created by PhpStorm.
 * User: lukaskovarik
 * Date: 16.04.18
 * Time: 17:21
 */

namespace App\Controller;

use App\Entity\Role;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\User;
use Symfony\Component\Translation\TranslatorInterface;


class UserController extends Controller
{
    /**
     * @return Response
     * @Route("/uzivatele", name="user_index")
     */
    public function indexAction(UserRepository $userRepository): Response
    {
        return $this->render('User/index.html.twig', [
            'users' => $userRepository->getUsers()
        ]);
    }

    /**
     * Remove user
     * @param int $id User id
     * @return Response HTTP response
     * @Route("/odstranit/{id}", name="remove_user")
     */
    public function removeAction(int $id, UserRepository $userRepository, TranslatorInterface $translator): Response
    {
        $userRepository->removeUser($id);
        $this->addFlash('notice', $translator->trans('user.was.successfully'));
        return $this->redirectToRoute('user_index');
    }

    /**
     * User editor by ID
     * @param null|int $id User id
     * @param Request $request HTTP request
     * @return Response HTTP response
     * @Route("/editor/{id}", name="user_editor")
     */
    public function editorAction(int $id = null, Request $request, UserRepository $userRepository, TranslatorInterface $translator): Response
    {
        // Find user by ID
        if ($id && !($user = $userRepository->getUser($id)))
            $this->addFlash('warning', 'Uživatel nebyl nalezen.');

        // If add new user
        if (empty($user)) $user = new User();

        /** @var Form $form */
        $form = $this->createFormBuilder($user)
            ->add('name', null, ['label' => $translator->trans('name')])
            ->add('email', null, ['label' => $translator->trans('email')])
            ->add('note', null, ['label' => $translator->trans('note')])
            ->add('role', EntityType::class, [
                'label' => $translator->trans('role'),
                'class' => Role::class,
                'choice_label' => 'name'
            ])
            ->add('submit', SubmitType::class, ['label' => $translator->trans('save.user')])
            ->getForm();

        // Submit form
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $userRepository->saveUser($user);
                $this->addFlash('notice', $translator->trans('user.was.successfully.saved'));
                return $this->redirectToRoute('user_index');
            } catch (UniqueConstraintViolationException $exception) {
                $this->addFlash('warning', $translator->trans('saving.error'));
            }
        }

        // Render view
        return $this->render('User/editor.html.twig', [
            'form' => $form->createView()
        ]);
    }
}