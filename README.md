# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

git clone https://lkovarik11@bitbucket.org/lkovarik11/rcsystem.git
cd rcsystem
composer install

Set db connection in .env

php bin/console server:start
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force

run in browser (default) - http://127.0.0.1:8000/

